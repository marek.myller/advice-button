let adviceId = document.querySelector('.id-nr');
let adviceTxt = document.querySelector('.advice');
const btn = document.querySelector('.btn');
const url = 'https://api.adviceslip.com/advice';

async function getAdvice() {
  const res = await fetch(url);
  const {
    slip: { id, advice },
  } = await res.json();
  adviceId.innerText = id;
  adviceTxt.innerText = advice;
}

getAdvice();

btn.addEventListener('click', getAdvice);
